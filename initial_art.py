import turtle

def initial_art():
    window = turtle.Screen()

    brad = turtle.Turtle()
    brad.shape("turtle")
    brad.color("blue")
    brad.speed(2)
    brad.right(60)
    brad.forward(100)
    brad.right(180)
    brad.forward(100)
    brad.left(120)
    brad.forward(100)
    brad.right(180)
    brad.forward(50)
    brad.right(60)
    brad.forward(50)
    brad.right(60)
    brad.forward(50)
    brad.left(60)

    brad.color("white")

    brad.forward(100)

    brad.color("blue")
    brad.left(90)
    brad.forward(90)
    brad.right(120)
    brad.forward(45)
    brad.left(60)
    brad.forward(45)
    brad.right(120)
    brad.forward(90)

    
    
    window.exitonclick()

initial_art()
