import os

def rename_file():
    #get the list of the files
    file_list  = os.listdir(r"G:\prank")
    saved_path = os.getcwd()
    print(saved_path)
    os.chdir(r"G:\prank")
    print(file_list)

    #rename the file names
    for file_name in file_list:
        print("Old name of the File:" + file_name)
        print("New name of the File:" + file_name.translate({ord(i): None for i in "0123456789"}))
        os.rename(file_name,file_name.translate({ord(i): None for i in "0123456789"}))
        
    os.chdir(saved_path)

rename_file()
