import turtle

def draw_rhombus(brad):
    brad.forward(50)
    brad.right(60)
    brad.forward(50)
    brad.right(120)
    brad.forward(50)
    brad.right(60)
    brad.forward(50)
    brad.right(120)

def draw_art():
    window = turtle.Screen()

    brad = turtle.Turtle()
    brad.shape("turtle")
    brad.color("blue")
    brad.speed(7)
    for i in range(1,49):
        draw_rhombus(brad)
        brad.right(7.5)
    brad.right(90)
    brad.forward(200)
    window.exitonclick()

draw_art()
     
