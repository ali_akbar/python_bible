import turtle

def draw_square(a_turtle):
    for i in range(1,5):
     a_turtle.forward(100)
     a_turtle.right(90)

def draw_art():
    window = turtle.Screen()
    window.bgcolor("red")

    brad = turtle.Turtle()
    brad.shape("circle")
    brad.color("yellow")
    brad.speed(7)
    draw_square(brad)
    for i in range(1,48):
     brad.right(7.5)
     draw_square(brad)

    window.exitonclick()

draw_art()
