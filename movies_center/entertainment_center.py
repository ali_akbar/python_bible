import fresh_tomatoes
import movies

toy_story = movies.Movie("Toy Story","A story about a boy and his toys that come to life","https://upload.wikimedia.org/wikipedia/en/4/4c/Toy_Story_4_poster.jpg","https://www.youtube.com/watch?v=TxtZhhCJkIY")

#print(toy_story.storyline)

avatar = movies.Movie("Avatar","A Marine on an alien planet.","https://upload.wikimedia.org/wikipedia/sco/b/b0/Avatar-Teaser-Poster.jpg","https://www.youtube.com/watch?v=5PSNL1qE6VY")

#print(avatar.storyline)

ms_dhoni = movies.Movie("MS DHONI","A Biography of the Greatest Captain of Indian Cricket Team.","https://upload.wikimedia.org/wikipedia/en/3/33/M.S._Dhoni_-_The_Untold_Story_poster.jpg","https://www.youtube.com/watch?v=6L6XqWoS8tw")

#print(ms_dhoni.storyline)

#toy_story.show_trailer()

school_of_rock = movies.Movie("School Of Rock","Using Rock Music to learn.","https://upload.wikimedia.org/wikipedia/en/1/11/School_of_Rock_Poster.jpg","https://www.youtube.com/watch?v=XCwy6lW5Ixc")

midnight_in_paris = movies.Movie("Midnight In Paris","Going back in time to meet Authors.","https://upload.wikimedia.org/wikipedia/en/9/9f/Midnight_in_Paris_Poster.jpg","https://www.youtube.com/watch?v=FAfR8omt-CY")

hunger_games = movies.Movie("Hunger Games","A really real reality show","https://upload.wikimedia.org/wikipedia/en/9/9d/Mockingjay_Part_2_Poster.jpg","https://www.youtube.com/watch?v=4S9a5V9ODuY")

movie = [toy_story,avatar,ms_dhoni,school_of_rock,midnight_in_paris,hunger_games]
#fresh_tomatoes.open_movies_page(movie)

print(movies.Movie.__doc__)
print(movies.Movie.__init__.__doc__)
print(movies.Movie.__name__)
print(movies.Movie.__init__.__name__)
print(movies.Movie.__module__)
print(movies.Movie.__init__.__module__)
